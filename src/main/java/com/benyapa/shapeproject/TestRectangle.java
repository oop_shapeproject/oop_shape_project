/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeproject;

/**
 *
 * @author bwstx
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(2,4);
        System.out.println("Area of rectangle1(width = "+ rectangle1.getWidth()+" length = "+rectangle1.getLength()+") is " + rectangle1.calArea());
        rectangle1.setLength(0);
        rectangle1.setWidth(4);
        System.out.println("Area of rectangle1(width = "+ rectangle1.getWidth()+" length = "+rectangle1.getLength()+") is " + rectangle1.calArea());
    }
    
}
