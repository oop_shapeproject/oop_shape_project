/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeproject;

/**
 *
 * @author bwstx
 */
public class Triangle {
    private double base;
    private double height;
    
    public Triangle(double base, double height){
        this.base = base;
        this.height = height;
    }
    public double calArea(){
        return 0.5*base*height;
    }
    public double getBase(){
        return base;
    }
    public void setBase(double base){
        if(base<=0){
            System.out.println("Error : base must more than zero!!!");
            return;
        }
        this.base = base;
    }
    public double getHeight(){
        return height;
    }
    public void setHeight(double height){
         if(height<=0){
            System.out.println("Error : height must more than zero!!!");
            return;
        }
        this.height = height;
    }
}
