/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.shapeproject;

/**
 *
 * @author bwstx
 */
public class Square {
    private double width;
    
    public Square(double width){
        this.width = width;
    }
    public double calArea(){
        return width*width;
    }
    public double getWidth(){
        return width;
    }
    public void setWidth(double width){
        if(width<=0){
            System.out.println("Error : Width must more than zero!!!");
            return;
        }
        this.width = width;
    }
}
